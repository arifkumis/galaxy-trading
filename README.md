# galaxy-trading


[![Build Status](https://travis-ci.org/codecentric/springboot-sample-app.svg?branch=master)](https://travis-ci.org/codecentric/springboot-sample-app)
[![Coverage Status](https://coveralls.io/repos/github/codecentric/springboot-sample-app/badge.svg?branch=master)](https://coveralls.io/github/codecentric/springboot-sample-app?branch=master)
[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

Galaxy Trading code challenge app.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Testing the application
For testing the application, you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:                                

 ```
mvn test
 ```

## Running the application
There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.galaxy.trading.GalaxyTradingApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

 ```
mvn spring-boot:run
 ```

Or. you can simply run available jar file here using command :

 ```
java -jar galaxy-trading-0.0.1-SNAPSHOT.jar
 ```

----------------------------------------------------
## Analyses
This program is consist for 4 main method. 2 method for get data value, 2 method for calculate data and return response message.
Here the list of all main method :

* **getRomanValue** - This method handling all inputed roman value. for example *'prok is V'*, it will save data *prok* as aliases for roman *V* on *romanMap* object.

* **getCreditValue** - This method will calculate how much value for some mineral data. for example *'glob glob Silver is 34 Credits'*, it will calculate how much value for  *Silver*. after get that value, it will be saved on *creditMap* object.

* **calculateRoman** - This method will calculate how much value for inputted roman alias. for example *'how much is pish tegj glob glob ?'*

* **calculateRomanAndMineral** - This method will calculate how many Credits for inputted combination between roman alias and credit alias. for example *'how many Credit is glob prok Iron ?'*

* **handlingCommand** -  This method will handling all instruction line that entered by user. including error handling if there are any unrecognized command.


## Explanation
*getRomanValue*

* User enter getRomanValue instruction, system will split that String by space. then it length will be checked. if the length is not 3, it will be throw an exception.

* After that, system will get roman and alias based on that Array String. alias is on index 0, roman is on index 2.<br/>

* If roman is not exist on accepted roman list, system will throw an exception.<br/>

* Roman and alias will be put into **romanMap**

*getCreditValue*

* User enter getCreditValue instruction, system will split that String by space. then it length will be checked. if the length is not 6, it will be throw an exception.

* The creditValue instruction is consist from 2 roman value, and 1 mineral value. it will be calculate to get total Credits.

* System job is to find how much mineral value.

* System will be calculated how much roman value. is that roman value will be subtracted first, or is that roman value will be added?

* After finish calculated roman value. system will check how much mineral value with formula **((W + X) *  Y = Z)**.

* System already get **(W + X)** and **Z** value. so the step to get **Y** value is divide **Z** with **(W + X)** value

* After get the result, system put mineral alias and value into "creditMap"

*calculateRoman*

* Calculate roman will be calculate all inputted roman alias and return the result to user.

*calculateRomanAndMineral*

* Calculate roman adn minerall will be calculate all inputted roman alias and mineral. after get the result, it will be returned into user.

*handlingCommand*

* This method will be handling all inserted instruction line from user to be processed by system.

## Flow Diagram

Here all flow diagram for this apps :

![alt text](https://preview.ibb.co/nHGmwe/main_diagram.jpg)