package com.galaxy.trading.util;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final List<String> canRepeatRoman = Arrays.asList("I","X","C","M");
    public static final Integer maxRepeat = 3;
    public static final Integer totalRepeat = 4;

    public static final List<String> romanNumeric = Arrays.asList("I","V","X","L","C","D","M");

    public class ErrorMsg {
        public static final String defaultError = "I have no idea what you are talking about";
        public static final String creditInputError = "Credit input is not valid";
        public static final String romanInputError = "Roman input is not valid";
    }
}
