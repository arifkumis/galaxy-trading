package com.galaxy.trading.util;

import java.util.ArrayList;
import java.util.List;

public class ConversionUtil {
    public static int romanVal(String text) {
        switch (text) {
            case "I":
                return 1;
            case "V":
                return 5;
            case "X":
                return 10;
            case "L":
                return 50;
            case "C":
                return 100;
            case "D":
                return 500;
            case "M":
                return 1000;
            default:
                throw new RuntimeException("Not recognized roman numeral");
        }
    }

    public static boolean canBeSubtracted(String first, String second) {
        boolean canBeSubtract = false;
        switch (first) {
            case "I":
                if (second.equals("V") || second.equals("X")) canBeSubtract = true;
                break;
            case "X":
                if (second.equals("L") || second.equals("C")) canBeSubtract = true;
                break;
            case "C":
                if (second.equals("D") || second.equals("M")) canBeSubtract = true;
                break;
        }
        return canBeSubtract;
    }

    public static double calculateRomanVal(String roman1, String roman2) {
        if (canBeSubtracted(roman1, roman2)) {
            return (romanVal(roman2) - romanVal(roman1));
        } else {
            return romanVal(roman1) + romanVal(roman2);
        }
    }

    public static String removeWhitespace(String text) {
        StringBuilder returnData = new StringBuilder();
        StringBuilder sb = new StringBuilder();
        for (Character c : text.trim().toCharArray()) {
            if (c.equals(' ')) {
                if (sb.length() > 0) {
                    returnData.append(sb.toString()).append(" ");
                }
                sb = new StringBuilder();
            } else {
                sb.append(c);
            }
        }
        if (sb.length() > 0) {
            returnData.append(sb.toString()).append(" ");
        }

        // remove last whitespace
        if (returnData.length() > 0) {
            returnData.setLength(returnData.length() - 1);
        }
        return returnData.toString();
    }
}
