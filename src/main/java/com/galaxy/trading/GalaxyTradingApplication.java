package com.galaxy.trading;

import com.galaxy.trading.service.TradingService;
import com.galaxy.trading.util.ConversionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

import static com.galaxy.trading.util.Constants.ErrorMsg.defaultError;
import static com.galaxy.trading.util.ConversionUtil.removeWhitespace;

@SpringBootApplication
public class GalaxyTradingApplication implements CommandLineRunner {
	private final TradingService tradingService;

	@Autowired
	public GalaxyTradingApplication(TradingService tradingService) {
		this.tradingService = tradingService;
	}

	public static void main(String[] args) {
		SpringApplication.run(GalaxyTradingApplication.class, args);
	}

	@Override
	public void run(String... strings) {
		System.out.println("Welcome into Galaxy Trading.");
		System.out.println("Please input your instruction line or type 'quit' to exit from this program.");
		while (true) {
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            String message = tradingService.handlingCommand(command);
            if (message != null) {
                System.out.println(message);
            }
		}
	}
}
