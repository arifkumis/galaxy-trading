package com.galaxy.trading.service;

public interface TradingService {

    void getRomanValue(String textLine);
    void getCreditValue(String textLine);
    String calculateRoman(String textLine);
    String calculateRomanAndMineral(String textLine);
    String handlingCommand(String command);
}
