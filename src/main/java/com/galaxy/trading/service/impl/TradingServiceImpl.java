package com.galaxy.trading.service.impl;

import com.galaxy.trading.service.TradingService;
import com.galaxy.trading.util.Constants;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static com.galaxy.trading.util.Constants.ErrorMsg.creditInputError;
import static com.galaxy.trading.util.Constants.ErrorMsg.defaultError;
import static com.galaxy.trading.util.Constants.ErrorMsg.romanInputError;
import static com.galaxy.trading.util.Constants.canRepeatRoman;
import static com.galaxy.trading.util.Constants.romanNumeric;
import static com.galaxy.trading.util.Constants.totalRepeat;
import static com.galaxy.trading.util.ConversionUtil.*;
import static java.lang.Integer.valueOf;

@Service
public class TradingServiceImpl implements TradingService {

    private Map<String, String> romanMap = new HashMap<>();
    private Map<String, Double> creditMap = new HashMap<>();

    @Override
    public void getRomanValue(String textLine) {
        // split text
        String[] romanInput = textLine.trim().split(" ");

        // check text lengt
        if (romanInput.length != 3) {
            throw new RuntimeException(romanInputError);
        }

        // get alias and roman
        String alias = romanInput[0];
        String roman = romanInput[2];

        // validate is correct roman symbol
        if (!romanNumeric.contains(roman)) {
            throw new RuntimeException(romanInputError);
        }

        // save to roman map
        if (!romanMap.containsKey(alias)) {
            romanMap.put(alias, roman);
        } else {
            romanMap.replace(alias, roman);
        }
    }

    @Override
    public void getCreditValue(String textLine) {
        // split text
        String[] textSplit = textLine.trim().split(" ");

        // validate text array
        if (textSplit.length != 6) {
            throw new RuntimeException(creditInputError);
        }

        // get roman alias, credit alias, and total credit
        String romanAlias1 = romanMap.getOrDefault(textSplit[0], null);
        String romanAlias2 = romanMap.getOrDefault(textSplit[1], null);
        String creditAlias = textSplit[2];
        double totalCredit = valueOf(textSplit[4]);
        double creditValue;

        // validate roman alias
        if (romanAlias1 == null || romanAlias2 == null) {
            throw new RuntimeException(creditInputError);
        }
        if (romanAlias1.length() != 1 || romanAlias2.length() != 1) {
            throw new RuntimeException(creditInputError);
        }

        // calculate roman value
        creditValue = calculateRomanVal(romanAlias1, romanAlias2);

        // get roman value
        creditValue = totalCredit / creditValue;

        // put into map
        if (!creditMap.containsKey(creditAlias)) {
            creditMap.put(creditAlias, creditValue);
        } else {
            creditMap.replace(creditAlias, creditValue);
        }
    }

    @Override
    public String calculateRoman(String textLine) {
        String command = textLine.trim().replace("how much is ", "");
        command = command.replace(" ?", "");
        return command + " is " + calculateData(command).intValue();
    }

    @Override
    public String calculateRomanAndMineral(String textLine) {
        String command = textLine.trim().replace("how many Credits is ", "");
        command = command.replace(" ?", "");
        return command + " is " + calculateData(command).intValue() + " Credits";
    }

    @Override
    public String handlingCommand(String command) {
        String result = null;
        try {
            command = removeWhitespace(command);
            String[] commandArray = command.trim().split(" ");

            if (command.equals("quit")) {
                System.exit(0);
            }
            else if (command.equals("reset")) {
                romanMap = new HashMap<>();
                creditMap = new HashMap<>();
                result = "Successfully reset data";
            }
            else if (command.contains("is")) {
                if (commandArray.length == 3) {
                    getRomanValue(command);
                }
                else if (command.endsWith("Credits")) {
                    getCreditValue(command);
                }
                else if (command.startsWith("how much is") && command.endsWith("?")) {
                    result = calculateRoman(command);
                }
                else if (command.startsWith("how many Credits") && command.endsWith("?")) {
                    result = calculateRomanAndMineral(command);
                }
                else {
                    result = defaultError;
                }
            } else {
                result = defaultError;
            }
        } catch (Exception e) {
            result = defaultError;
        }
        return result;
    }

    private Double calculateData(String data) {
        int totalRoman = 0;
        int difference = 0;
        String countedRoman = null;
        String lastRoman = null;

        String roman1 = null;
        String roman2 = null;
        Double totalValue = 0d;
        String romanAlias;

        for (String s : data.split(" ")) {
            if (romanMap.containsKey(s)) {
                romanAlias = romanMap.get(s);

                // process restriction of roman

                // handling if roman can be repeat
                if (canRepeatRoman.contains(romanAlias)) {

                    // check if roman alias is not same with last roman
                    if (!romanAlias.equals(lastRoman)) {
                        if (lastRoman != null) {
                            if (difference == 1) {
                                if (romanAlias.equals(countedRoman)) {
                                    // check is roman can be repeated with 4 max or not.
                                    if (romanVal(lastRoman) < romanVal(countedRoman)) {
                                        totalRoman ++;
                                        if (totalRoman > totalRepeat) {
                                            throw new RuntimeException("total roman repeated only can be 4 times");
                                        }
                                    } else {
                                        throw new RuntimeException("roman only can be repeated 4 times if they separated by smaller value");
                                    }
                                }
                            } else {
                                difference ++;
                            }
                        }
                    } else {
                        if (countedRoman == null) {
                            countedRoman = romanAlias;
                            totalRoman += 1;
                        }
                        totalRoman ++;

                        if (totalRoman > Constants.maxRepeat) {
                            throw new RuntimeException("Maximum repeated is only 3 times");
                        }
                    }
                } else {

                    // handling if roman can be repeat
                    if (romanAlias.equals(lastRoman)) {
                        throw new RuntimeException("roman cannot be repeated");
                    }
                }
                lastRoman = romanAlias;

                // calculate roman value
                if (roman1 == null) {
                    roman1 = romanAlias;
                } else if (roman2 == null) {
                    roman2 = romanAlias;
                } else {
                    if (canBeSubtracted(roman1, roman2)) {
                        totalValue += (romanVal(roman2) - romanVal(roman1));
                        roman1 = romanAlias;
                        roman2 = null;
                    } else {
                        totalValue += romanVal(roman1);
                        roman1 = roman2;
                        roman2 = romanAlias;
                    }
                }
            } else if (creditMap.containsKey(s)) {

                // handling if data is credit value, not roman value
                double creditValue = creditMap.get(s);
                if (roman1 != null && roman2 != null) {
                    if (canBeSubtracted(roman1, roman2)) {
                        totalValue += (romanVal(roman2) - romanVal(roman1));
                    } else {
                        totalValue += romanVal(roman1);
                        totalValue += romanVal(roman2);
                    }
                } else if (roman1 != null) {
                    totalValue += romanVal(roman1);
                }

                totalValue *= creditValue;
                roman1 = null;
                roman2 = null;
            } else {
                throw new RuntimeException("Unrecognized string value");
            }
        }

        if (roman1 != null && roman2 != null) {
            if (canBeSubtracted(roman1, roman2)) {
                totalValue += (romanVal(roman2) - romanVal(roman1));
            } else {
                totalValue += (romanVal(roman1) + romanVal(roman2));
            }
        } else if (roman1 != null) {
            totalValue += romanVal(roman1);
        }

        return totalValue;
    }
}
