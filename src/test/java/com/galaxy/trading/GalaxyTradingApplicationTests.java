package com.galaxy.trading;

import com.galaxy.trading.service.impl.TradingServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class GalaxyTradingApplicationTests {
    @InjectMocks
    private TradingServiceImpl tradingService;

    /**
     * list of functional test are :
     * - initialize data
     * - calculate data with roman only
     * - calculate data with roman and mineral data
     * - calculate data with unknown command
     * */
    @Test
    public void functionalTest() {
        String result;

        result = tradingService.handlingCommand("glob is I");
        assertNull(result);

        result = tradingService.handlingCommand("prok is V");
        assertNull(result);

        result = tradingService.handlingCommand("pish is X");
        assertNull(result);

        result = tradingService.handlingCommand("tegj is L");
        assertNull(result);

        result = tradingService.handlingCommand("glob glob Silver is 34 Credits");
        assertNull(result);

        result = tradingService.handlingCommand("glob prok Gold is 57800 Credits");
        assertNull(result);

        result = tradingService.handlingCommand("pish pish Iron is 3910 Credits");
        assertNull(result);

        result = tradingService.handlingCommand("how much is pish tegj glob glob ?");
        assertEquals("pish tegj glob glob is 42", result);

        result = tradingService.handlingCommand("how many Credits is glob prok Silver ?");
        assertEquals("glob prok Silver is 68 Credits", result);

        result = tradingService.handlingCommand("how many Credits is glob prok Gold ?");
        assertEquals("glob prok Gold is 57800 Credits", result);

        result = tradingService.handlingCommand("how many Credits is glob prok Iron ?");
        assertEquals("glob prok Iron is 782 Credits", result);

        result = tradingService.handlingCommand("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?");
        assertEquals("I have no idea what you are talking about", result);
    }

    /**
     * list of negative test are :
     * - initialize data
     * - calculate data with extra space
     * - calculate data with roman role
     * - calculate data with unknown command
     * */
    @Test
    public void negativeTest() {
        String result;
        String errorMessage = "I have no idea what you are talking about";

        result = tradingService.handlingCommand("glob is I");
        assertNull(result);

        result = tradingService.handlingCommand("prok is V");
        assertNull(result);

        result = tradingService.handlingCommand("pish is X");
        assertNull(result);

        result = tradingService.handlingCommand("tegj is L");
        assertNull(result);

        result = tradingService.handlingCommand("glob glob Silver is 34 Credits");
        assertNull(result);

        result = tradingService.handlingCommand("glob prok Gold is 57800 Credits");
        assertNull(result);

        result = tradingService.handlingCommand("pish pish Iron is 3910 Credits");
        assertNull(result);

        // test extra space
        result = tradingService.handlingCommand("   how       much is    pish  tegj   glob glob ?      ");
        assertEquals("pish tegj glob glob is 42", result);

        // test correct roman role
        result = tradingService.handlingCommand("how much is pish pish pish glob pish ?");
        assertTrue(result.contains("pish pish pish glob pish is"));

        // test incorrect roman role
        result = tradingService.handlingCommand("how much is pish pish pish glob pish pish?");
        assertEquals(errorMessage, result);

        // test incorrect roman role
        result = tradingService.handlingCommand("how much is pish pish pish glob pish glob pish?");
        assertEquals(errorMessage, result);

        // test roman that can be repeated
        result = tradingService.handlingCommand("how much is tegj tegj?");
        assertEquals(errorMessage, result);

        // test with incorrect command
        result = tradingService.handlingCommand("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?");
        assertEquals(errorMessage, result);
    }
}
